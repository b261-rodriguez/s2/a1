package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {

    public static void main(String[]args){
        Scanner yearScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year");

        int yearValue = yearScanner.nextInt();

        if (yearValue % 4 == 0 || yearValue % 100 != 0 && yearValue % 400 == 0){
            System.out.println(yearValue + " is a leap year");
        } else{
            System.out.println(yearValue + " is not a leap year");
        }


    }

}
