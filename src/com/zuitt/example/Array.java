package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // Java Collection
        // are a single unit of objects
        // usefull= for manipulating relevant pieces of data that can be used in different situation
    public static void main(String[] args){
        // Arrays
            // Array are containers of values of the SAME Data type given a pre-defined amount of values
            // Java arrays are more rigid, once the size and data type are are defined, they an no longer be change.

            //Syntax: Array Declaration
                //dataType[] identifier = new dataType[numOfElements]
                // the vales of the array is initializes to 0 or null

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;



        System.out.println(intArray[2]);
        //it prints the memory address of the array
        System.out.println(intArray);

        //To print the intArray
        System.out.println(Arrays.toString(intArray));

        boolean[] bArr = new boolean[2];

        System.out.println(Arrays.toString(bArr));

        // Declaring array with initialization

        //Syntax:
            //dataType[] identifier = {elementA, elementB, .... elementNth};

        String[] names = {"John", "Jane", "Joe"};

        System.out.println(Arrays.toString(names));
        System.out.println(names.length);

        //Sample Java Array Methods


        //Multi-dimensional Arrays
        //Syntax:
            //dataType[][] identifier = new dataType[rowLength][colLength]

        String [][] classroom = new String [3][3];

        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Armis";
        //Second ro
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.toString(classroom));
        //we use the deepToSting() method for printing multidimensional array

        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
            //are resizable arrays, wherein elements can be added or removed whenever it is needed

            //Syntax:
                //ArrayList<dataType> identifier = new ArrayList<dataType>()

//        //Declare an arraylist
//        ArrayList<String> students = new ArrayList<>();
//
//        //Adding elements to ArrayList is done by the add() function
//        students.add("John");
//        students.add("Paul");
//
//        System.out.println(students);

        //Declare an ArrayList with values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("John", "Paul"));


        System.out.println(students);


        //Accessing elements to an ArrayList is done by the get() function
        System.out.println(students.get(1));

        //Updating item in the ArrayList
        //arrayListName.set(index, element)
        students.set(1, "George");
        System.out.println(students);

        //inserting element in specific index
        students.add(1,"Mike");
        System.out.println(students);

        //Removing a specific element
        students.remove(1);
        System.out.println(students);

        //removing all elements
        students.clear();
        System.out.println(students);

        //getting the number of elements in an ArrayList
        System.out.println(students.size());

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(23, 42));
        System.out.println(numbers);

        //HashMaps
            //collection of data in "key-value pairs"
            //in Java, "keys" also referred by the "fields"

        //Syntax
            //HashMap<dataTypeField, dataTypeValue> identifier = new new HasMap<dataTypeField, dataTpeValue>();

        //Declaring HashMaps;
        HashMap<String,String> jobPosition1 = new HashMap<>();
        System.out.println(jobPosition1);

        //Add elements to HashMap by using the put() function
        jobPosition1.put("Student", "Alice");
        jobPosition1.put("Developer", "Magic");
        System.out.println(jobPosition1);

        HashMap<String,String> jobPosition2 = new HashMap<>(){
            {
                put("Student", "Alice");
                put("Developer", "Magic");

            }
        };
        System.out.println(jobPosition2);

        //Accessing elements in HashMaps
        System.out.println(jobPosition2.get("Developer"));

        //Updating an element
        jobPosition2.replace("Student","Jacob");
        System.out.println(jobPosition2);

        jobPosition2.put("Student", "Jacob P");
        System.out.println(jobPosition2);

        System.out.println(jobPosition2.keySet());
        System.out.println(jobPosition2.keySet());

        jobPosition2.remove("Student");
        System.out.println(jobPosition2);
















    }
}
